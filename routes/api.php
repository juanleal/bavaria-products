<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

// Versión 1.0 del API rest
Route::group([
    'prefix' => 'v1'
], function () {

    Route::post('login', 'v1\AuthController@login');
    Route::post('signup', 'v1\AuthController@signUp');

    Route::middleware(['auth:api'])->group(function () {
        Route::get('logout', 'v1\AuthController@logout');
        Route::get('user', 'v1\AuthController@user');
        
        Route::resource('clientes', v1\ClientesAPIController::class);
        Route::resource('productos', v1\ProductosAPIController::class);
        Route::resource('stocks', v1\StockAPIController::class);
        Route::resource('facturas', v1\FacturasAPIController::class);
    });
});
