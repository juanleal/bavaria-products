<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Requests\API\v1\CreateProductosAPIRequest;
use App\Http\Requests\API\v1\UpdateProductosAPIRequest;
use App\Models\Productos;
use App\Repositories\ProductosRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ProductosController
 * @package v1
 */

class ProductosAPIController extends AppBaseController
{
    /** @var  ProductosRepository */
    private $productosRepository;

    public function __construct(ProductosRepository $productosRepo)
    {
        $this->productosRepository = $productosRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/productos",
     *      summary="Get a listing of the Productos.",
     *      tags={"Productos"},
     *      description="Get all Productos",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Productos")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $productos = $this->productosRepository->allProducts()->get();

        return $this->sendResponse($productos->toArray(), 'Productos retrieved successfully');
    }

    /**
     * @param CreateProductosAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/productos",
     *      summary="Store a newly created Productos in storage",
     *      tags={"Productos"},
     *      description="Store Productos",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Productos that should be stored",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              required={"nombre", "referencia"},
     *              @SWG\Property(
     *                  property="nombre",
     *                  description="nombre",
     *                  type="string",
     *              ),
     *              @SWG\Property(
     *                  property="referencia",
     *                  description="referencia",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="descripcion",
     *                  description="descripcion",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Productos"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateProductosAPIRequest $request)
    {
        $input = $request->all();

        $productos = $this->productosRepository->create($input);

        return $this->sendResponse($productos->toArray(), 'Productos saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/productos/{id}",
     *      summary="Display the specified Productos",
     *      tags={"Productos"},
     *      description="Get Productos",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Productos",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Productos"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Productos $productos */
        $productos = $this->productosRepository->allProducts(['productos.id'=>$id])->first();

        if (empty($productos)) {
            return $this->sendError('Productos not found');
        }

        return $this->sendResponse($productos->toArray(), 'Productos retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateProductosAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/productos/{id}",
     *      summary="Update the specified Productos in storage",
     *      tags={"Productos"},
     *      description="Update Productos",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Productos",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Productos that should be updated",
     *          required=false,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="nombre",
     *                  description="nombre",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="referencia",
     *                  description="referencia",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="descripcion",
     *                  description="descripcion",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Productos"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateProductosAPIRequest $request)
    {
        $input = $request->all();

        /** @var Productos $productos */
        $productos = $this->productosRepository->find($id);

        if (empty($productos)) {
            return $this->sendError('Productos not found');
        }

        $productos = $this->productosRepository->update($input, $id);

        return $this->sendResponse($productos->toArray(), 'Productos updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/productos/{id}",
     *      summary="Remove the specified Productos from storage",
     *      tags={"Productos"},
     *      description="Delete Productos",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Productos",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Productos $productos */
        $productos = $this->productosRepository->find($id);

        if (empty($productos)) {
            return $this->sendError('Productos not found');
        }

        $productos->delete();

        return $this->sendSuccess('Productos deleted successfully');
    }
}
