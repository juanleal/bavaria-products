<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Requests\API\v1\CreateStockAPIRequest;
use App\Http\Requests\API\v1\UpdateStockAPIRequest;
use App\Models\Stock;
use App\Repositories\StockRepository;
use App\Repositories\ProductosRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class StockController
 * @package App\Http\Controllers\API\v1
 */

class StockAPIController extends AppBaseController
{
    /** @var  StockRepository */
    private $stockRepository;

    /** @var  ProductosRepository */
    private $productosRepository;

    public function __construct(StockRepository $stockRepo, ProductosRepository $productosRepo)
    {
        $this->stockRepository = $stockRepo;
        $this->productosRepository = $productosRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/stocks",
     *      summary="Get a listing of the Stocks.",
     *      tags={"Stock"},
     *      description="Get all Stocks",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="object",
     *                  @SWG\Property(
     *                      property="sku",
     *                      type="array",
     *                      @SWG\Items(ref="#/definitions/Stock")
     *                  ),
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $stocks = $this->stockRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        )
        ->groupBy('sku');

        return $this->sendResponse($stocks->toArray(), 'Stocks retrieved successfully');
    }

    /**
     * @param CreateStockAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/stocks",
     *      summary="Store a newly created Stock in storage",
     *      tags={"Stock"},
     *      description="Store Stock",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Stock that should be stored",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              required={"product_id", "cantidad", "precio", "iva"},
     *              @SWG\Property(
     *                  property="product_id",
     *                  description="nombre",
     *                  type="integer",
     *                  format="int32"
     *              ),
     *              @SWG\Property(
     *                  property="sku",
     *                  description="sku",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="cantidad",
     *                  description="cantidad",
     *                  type="integer",
     *                  format="int32"
     *              ),
     *              @SWG\Property(
     *                  property="precio",
     *                  description="precio",
     *                  type="number",
     *                  format="number"
     *              ),
     *              @SWG\Property(
     *                  property="url_foto",
     *                  description="url_foto",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="iva",
     *                  description="iva",
     *                  type="number",
     *                  format="number"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Stock"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateStockAPIRequest $request)
    {
        $input = $request->all();

        $producto = $this->productosRepository->find($request->product_id);
        if(!$request->has('sku') || $request->sku == ""){
            $input['sku'] = $producto->id."-".$producto->referencia."-".($producto->stocks->count()+1);
            $stock = $this->stockRepository->create($input);
        } else {
            $stockBase = $this->stockRepository->allQuery(['sku' => $request->sku])->first();
            $stock = $stockBase->replicate();
            $stock->cantidad = $request->cantidad;
            $stock->save();
        }

        return $this->sendResponse($stock->toArray(), 'Stock saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/stocks/{id}",
     *      summary="Display the specified Stock",
     *      tags={"Stock"},
     *      description="Get Stock",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Stock",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Stock"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Stock $stock */
        $stock = $this->stockRepository->find($id);

        if (empty($stock)) {
            return $this->sendError('Stock not found');
        }

        return $this->sendResponse($stock->toArray(), 'Stock retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateStockAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/stocks/{id}",
     *      summary="Update the specified Stock in storage",
     *      tags={"Stock"},
     *      description="Update Stock",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Stock",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Stock that should be updated",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              required={"product_id", "cantidad", "precio", "iva"},
     *              @SWG\Property(
     *                  property="cantidad",
     *                  description="cantidad",
     *                  type="integer",
     *                  format="int32"
     *              ),
     *              @SWG\Property(
     *                  property="precio",
     *                  description="precio",
     *                  type="number",
     *                  format="number"
     *              ),
     *              @SWG\Property(
     *                  property="url_foto",
     *                  description="url_foto",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="iva",
     *                  description="iva",
     *                  type="number",
     *                  format="number"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Stock"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateStockAPIRequest $request)
    {
        $input = $request->all();

        /** @var Stock $stock */
        $stock = $this->stockRepository->find($id);

        if (empty($stock)) {
            return $this->sendError('Stock not found');
        }

        $stock = $this->stockRepository->update($input, $id);

        return $this->sendResponse($stock->toArray(), 'Stock updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/stocks/{id}",
     *      summary="Remove the specified Stock from storage",
     *      tags={"Stock"},
     *      description="Delete Stock",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Stock",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Stock $stock */
        $stock = $this->stockRepository->find($id);

        if (empty($stock)) {
            return $this->sendError('Stock not found');
        }

        $stock->delete();

        return $this->sendSuccess('Stock deleted successfully');
    }
}
