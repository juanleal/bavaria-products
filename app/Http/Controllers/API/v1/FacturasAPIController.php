<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Requests\API\v1\CreateFacturasAPIRequest;
use App\Http\Requests\API\v1\UpdateFacturasAPIRequest;
use App\Models\Facturas;
use App\Models\ItemsFactura;
use App\Repositories\FacturasRepository;
use App\Repositories\StockRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Validator;
use Response;

/**
 * Class FacturasController
 * @package App\Http\Controllers\API\v1
 */

class FacturasAPIController extends AppBaseController
{
    /** @var  FacturasRepository */
    private $facturasRepository;

    /** @var  StockRepository */
    private $stockRepository;

    public function __construct(StockRepository $stockRepo, FacturasRepository $facturasRepo)
    {
        $this->stockRepository = $stockRepo;
        $this->facturasRepository = $facturasRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/facturas",
     *      summary="Get a listing of the Facturas.",
     *      tags={"Facturas"},
     *      description="Get all Facturas",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Facturas")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $facturas = $this->facturasRepository->allFacturas(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        )->get();

        return $this->sendResponse($facturas->toArray(), 'Facturas retrieved successfully');
    }

    /**
     * @param CreateFacturasAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/facturas",
     *      summary="Store a newly created Facturas in storage",
     *      tags={"Facturas"},
     *      description="Store Facturas",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Facturas that should be stored",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              required={"clientes_id", "items"},
     *              @SWG\Property(
     *                  property="clientes_id",
     *                  description="cliente id",
     *                  type="integer",
     *                  format="int32"
     *              ),
     *              @SWG\Property(
     *                  property="items",
     *                  description="sku items",
     *                  type="array",
     *                  @SWG\Items(
     *                      @SWG\Property(
     *                         property="sku",
     *                         description="sku product stock",
     *                         type="string",
     *                         example="1-142577-2"
     *                     ),
     *                     @SWG\Property(
     *                         property="cantidad",
     *                         description="cantidad por producto",
     *                         type="integer",
     *                         format="int32",
     *                         example="1"
     *                     )
     *                  )
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Facturas"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateFacturasAPIRequest $request)
    {
        $input = $request->all();
        
        $skuItems = [];
        $rules = $this->generateItemsRulesArray($input['items'], $skuItems);

        $messages = [
            'lte' => 'The SKU :attribute exceeds the number of products available which is :value.'
        ];
        $validator = Validator::make($skuItems, $rules, $messages)->validate();

        $facturas = $this->facturasRepository->create($input);

        $facturas->items()->saveMany($this->convertArrayItemsFacturas($input['items']));

        $factura = $this->facturasRepository->allFacturas(['facturas.id'=>$facturas->id])->first();
        return $this->sendResponse($factura->toArray(), 'Facturas saved successfully');
    }

    /**
     * @param int $items
     * @return Array
     * Se generan unas reglas para que no se exceda la compra de los productos disponibles
     */
    public function generateItemsRulesArray($items, &$skuItems, $factura = NULL){
        $rules = [];
        $arrayItems = [];
        if($factura){
            $arrayItems = $factura->items->collect();
        }
        foreach ($items as $item) {
            $findItem = $this->findItemBySku($arrayItems, $item['sku']);
            $sku = !empty($findItem) ? $findItem : null;
            $stock = $this->stockRepository->getDisponible(['stock.sku' => $item['sku']])->first();
            $rules[$item['sku']] = $sku != null ?  'lte:' . ($stock->disponibles + $sku->cantidad) :  'lte:' . $stock->disponibles;
            $skuItems[$item['sku']] = $item['cantidad'];
            
        }
        return $rules;
    }

    /**
     * @param int $items
     * @return Array
     * Busca un sku en un array
     */
    public function findItemBySku($items, $sku){
        $itemFound = [];
        foreach ($items as $item) {
            if($item['sku'] == $sku){
                $itemFound = $item;
            }
            
        }
        return $itemFound;
    }

    /**
     * @param int $items
     * @return Array
     * Se genera un array de objetos tipo ItemsFactura para poder asociarlo a una factura
     */
    public function convertArrayItemsFacturas($items){
        $arrayItems = [];
        foreach ($items as $item) {
            $arrayItems[] = $this->getItemFacturaBySKU($item);
            
        }
        return $arrayItems;
    }
    
    /**
     * @param int $items
     * @return ItemsFactura
     * Crea un objeto de tipo ItemsFactura
     */
    public function getItemFacturaBySKU($item){
        $stock = $this->stockRepository->allQuery(['sku' => $item['sku']])->first();
        $itemFactura = new ItemsFactura();
        $itemFactura->sku = $item['sku'];
        $itemFactura->cantidad = $item['cantidad'];
        $itemFactura->valor_unitario_producto = $stock->precio;
        $itemFactura->iva = $stock->iva;
        $itemFactura->subtotal_productos = $stock->precio * $item['cantidad'];
        $itemFactura->valor_total_productos = $itemFactura->subtotal_productos + ($itemFactura->subtotal_productos * $stock->iva);
        return $itemFactura;
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/facturas/{id}",
     *      summary="Display the specified Facturas",
     *      tags={"Facturas"},
     *      description="Get Facturas",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Facturas",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Facturas"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Facturas $facturas */
        $facturas = $this->facturasRepository->allFacturas(['facturas.id'=>$id])->first();

        if (empty($facturas)) {
            return $this->sendError('Facturas not found');
        }

        return $this->sendResponse($facturas->toArray(), 'Facturas retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateFacturasAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/facturas/{id}",
     *      summary="Update the specified Facturas in storage",
     *      tags={"Facturas"},
     *      description="Update Facturas",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Facturas",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Facturas that should be updated",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              required={"clientes_id", "items"},
     *              @SWG\Property(
     *                  property="clientes_id",
     *                  description="cliente id",
     *                  type="integer",
     *                  format="int32"
     *              ),
     *              @SWG\Property(
     *                  property="items",
     *                  description="sku items",
     *                  type="array",
     *                  @SWG\Items(
     *                      @SWG\Property(
     *                         property="sku",
     *                         description="sku product stock",
     *                         type="string",
     *                         example="1-142577-2"
     *                     ),
     *                     @SWG\Property(
     *                         property="cantidad",
     *                         description="cantidad por producto",
     *                         type="integer",
     *                         format="int32",
     *                         example="1"
     *                     )
     *                  )
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Facturas"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateFacturasAPIRequest $request)
    {
        $input = $request->all();

        /** @var Facturas $facturas */
        $facturas = $this->facturasRepository->find($id);

        if (empty($facturas)) {
            return $this->sendError('Facturas not found');
        }

        $skuItems = [];
        $rules = $this->generateItemsRulesArray($input['items'], $skuItems, $facturas);
        
        $messages = [
            'lte' => 'The SKU :attribute exceeds the number of products available which is :value.'
        ];
        $validator = Validator::make($skuItems, $rules, $messages)->validate();

        $facturas = $this->facturasRepository->update($input, $id);
        $facturas->items()->delete();

        $facturas->items()->saveMany($this->convertArrayItemsFacturas($input['items']));

        $factura = $this->facturasRepository->allFacturas(['facturas.id'=>$facturas->id])->first();
        
        return $this->sendResponse($factura->toArray(), 'Facturas updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/facturas/{id}",
     *      summary="Remove the specified Facturas from storage",
     *      tags={"Facturas"},
     *      description="Delete Facturas",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Facturas",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Facturas $facturas */
        $facturas = $this->facturasRepository->find($id);

        if (empty($facturas)) {
            return $this->sendError('Facturas not found');
        }

        $facturas->delete();

        return $this->sendSuccess('Facturas deleted successfully');
    }
}
