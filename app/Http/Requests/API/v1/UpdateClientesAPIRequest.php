<?php

namespace App\Http\Requests\API\v1;

use App\Models\Clientes;
use InfyOm\Generator\Request\APIRequest;

class UpdateClientesAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nombres' => 'nullable|string|max:50',
            'apellidos' => 'nullable|string|max:50',
            'documento' => 'nullable|string|max:20|unique:clientes,documento,' . $this->cliente,
            'telefono' => 'nullable|string|max:50',
            'email' => 'nullable|string|max:50|unique:clientes,email,' . $this->cliente,
            'direccion' => 'nullable|string|max:100'
        ];
        
        return $rules;
    }
}
