<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @SWG\Definition(
 *      definition="Productos",
 *      required={"nombre", "referencia"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="nombre",
 *          description="nombre",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="referencia",
 *          description="referencia",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="descripcion",
 *          description="descripcion",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="stocks",
 *          description="stocks",
 *          type="array",
 *          @SWG\Items(
 *              @SWG\Property(
 *                  property="product_id",
 *                  description="product id",
 *                  type="integer",
 *                  format="int32"
 *              ),
 *              @SWG\Property(
 *                  property="sku",
 *                  description="sku id",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="precio",
 *                  description="precio",
 *                  type="number",
 *                  format="number"
 *              ),
 *              @SWG\Property(
 *                  property="url_foto",
 *                  description="Url foto",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="iva",
 *                  description="iva",
 *                  type="number",
 *                  format="number"
 *              ),
 *              @SWG\Property(
 *                  property="disponibles",
 *                  description="disponibles",
 *                  type="integer",
 *                  format="int32"
 *              )
 *          )
 *      )
 * )
 */
class Productos extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'productos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'nombre',
        'referencia',
        'descripcion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombre' => 'string',
        'referencia' => 'string',
        'descripcion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required|string|max:100',
        'referencia' => 'required|string|max:50|unique:productos',
        'descripcion' => 'nullable|string',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function stocks()
    {
        return $this->hasMany(\App\Models\Stock::class, 'product_id');
    }
}
