<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ItemsFactura extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'items_factura';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'facturas_id',
        'sku',
        'cantidad',
        'valor_unitario_producto',
        'iva',
        'subtotal_productos',
        'valor_total_productos'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function facturas()
    {
        return $this->belongsTo(\App\Models\Facturas::class, 'facturas_id');
    }
}
