<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @SWG\Definition(
 *      definition="Facturas",
 *      required={"clientes_id", "created_at", "updated_at"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="clientes_id",
 *          description="clientes_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="subtotal",
 *          description="subtotal",
 *          type="number"
 *      ),
 *      @SWG\Property(
 *          property="total",
 *          description="total",
 *          type="number"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="cliente",
 *          description="cliente de la factura",
 *          type="array",
 *          @SWG\Items(ref="#/definitions/Clientes")
 *      ),
 *      @SWG\Property(
 *          property="items",
 *          description="items de la factura",
 *          type="array",
 *          @SWG\Items(
 *              @SWG\Property(
 *                  property="facturas_id",
 *                  description="id factura",
 *                  type="integer",
 *                  format="int32"
 *              ),
 *              @SWG\Property(
 *                  property="sku_item",
 *                  description="sku product item",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="cantidad",
 *                  description="cantidad",
 *                  type="integer",
 *                  format="int32"
 *              ),
 *              @SWG\Property(
 *                  property="valor_unitario_producto",
 *                  description="valor unitario producto",
 *                  type="number",
 *                  format="number"
 *              ),
 *              @SWG\Property(
 *                  property="iva_producto",
 *                  description="iva del producto",
 *                  type="number",
 *                  format="number"
 *              ),
 *              @SWG\Property(
 *                  property="subtotal_productos",
 *                  description="subtotal producto",
 *                  type="number",
 *                  format="number"
 *              ),
 *              @SWG\Property(
 *                  property="valor_total_productos",
 *                  description="valor total producto",
 *                  type="number",
 *                  format="number"
 *              ),
 *              @SWG\Property(
 *                  property="created_at",
 *                  description="created_at",
 *                  type="string",
 *                  format="date-time"
 *              ),
 *              @SWG\Property(
 *                  property="updated_at",
 *                  description="updated_at",
 *                  type="string",
 *                  format="date-time"
 *              ),
 *              @SWG\Property(
 *                  property="deleted_at",
 *                  description="deleted_at",
 *                  type="string",
 *                  format="date-time"
 *              )
 *          )
 *      )
 * )
 */
class Facturas extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'facturas';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'clientes_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'clientes_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'clientes_id' => 'required|integer|exists:clientes,id',
        'items' => 'required|array',
        'items.*.sku' => 'string|exists:stock,sku',
        'items.*.cantidad' => 'required|integer|min:1',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function cliente()
    {
        return $this->belongsTo(\App\Models\Clientes::class, 'clientes_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function items()
    {
        return $this->hasMany(\App\Models\ItemsFactura::class, 'facturas_id');
    }
}
