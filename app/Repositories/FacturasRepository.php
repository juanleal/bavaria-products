<?php

namespace App\Repositories;

use App\Models\Facturas;
use App\Repositories\BaseRepository;

/**
 * Class FacturasRepository
 * @package App\Repositories
 * @version December 13, 2020, 4:00 pm UTC
*/

class FacturasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'facturas.id',
        'clientes_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Facturas::class;
    }
    
    /**
     * Get facturas with items
     **/
    public function allFacturas($search = [], $skip = null, $limit = null)
    {
        $query = $this->model->newQuery();
        $query->selectRaw('facturas.*, SUM(subtotal_productos) as subtotal, SUM(valor_total_productos) as total')->join('items_factura', function($join){
            $join->on('facturas.id', '=', 'items_factura.facturas_id');
        })->with(['cliente','items'])->groupBy('facturas.id')->whereNull('items_factura.deleted_at');

        if (count($search)) {
            foreach($search as $key => $value) {
                if (in_array($key, $this->getFieldsSearchable())) {
                    $query->where($key, $value);
                }
            }
        }

        if (!is_null($skip)) {
            $query->skip($skip);
        }

        if (!is_null($limit)) {
            $query->limit($limit);
        }

        return $query;
    }
}
