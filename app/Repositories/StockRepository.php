<?php

namespace App\Repositories;

use App\Models\Stock;
use App\Repositories\BaseRepository;

/**
 * Class StockRepository
 * @package App\Repositories
 * @version December 13, 2020, 4:03 am UTC
*/

class StockRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'sku',
        'stock.sku',
        'cantidad',
        'precio',
        'url_foto',
        'iva'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Stock::class;
    }
    
    /**
     * Get products with stock
     **/
    public function getDisponible($search = [], $skip = null, $limit = null)
    {
        $query = $this->model->newQuery();
        $query->selectRaw("(select case when (sum(stock.cantidad) - sum(items_factura.cantidad)) is null then sum(stock.cantidad) else (sum(stock.cantidad) - sum(items_factura.cantidad)) end as disponibles from items_factura where items_factura.sku = stock.sku and deleted_at is null) as disponibles")
        ->groupBy('product_id','stock.sku', 'stock.precio', 'url_foto', 'stock.iva');

        if (count($search)) {
            foreach($search as $key => $value) {
                if (in_array($key, $this->getFieldsSearchable())) {
                    $query->where($key, $value);
                }
            }
        }

        if (!is_null($skip)) {
            $query->skip($skip);
        }

        if (!is_null($limit)) {
            $query->limit($limit);
        }

        return $query;
    }
}
