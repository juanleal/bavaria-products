<?php

namespace App\Repositories;

use App\Models\Productos;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

/**
 * Class ProductosRepository
 * @package App\Repositories
 * @version December 13, 2020, 3:22 am UTC
*/

class ProductosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'productos.id',
        'nombre',
        'referencia',
        'descripcion',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Productos::class;
    }

    /**
     * Get products with stock
     **/
    public function allProducts($search = [], $skip = null, $limit = null)
    {
        $query = $this->model->newQuery();
        $query->with('stocks', function ($query) {
            $query->selectRaw("product_id, stock.sku as sku, (stock.precio + (stock.precio * stock.iva)) as precio, url_foto, stock.iva as iva, (select case when (sum(stock.cantidad) - sum(items_factura.cantidad)) is null then sum(stock.cantidad) else (sum(stock.cantidad) - sum(items_factura.cantidad)) end as disponibles from items_factura where items_factura.sku = stock.sku and deleted_at is null) as disponibles")
            ->groupBy('product_id','stock.sku', 'stock.precio', 'url_foto', 'stock.iva');
        });

        if (count($search)) {
            foreach($search as $key => $value) {
                if (in_array($key, $this->getFieldsSearchable())) {
                    $query->where($key, $value);
                }
            }
        }

        if (!is_null($skip)) {
            $query->skip($skip);
        }

        if (!is_null($limit)) {
            $query->limit($limit);
        }

        return $query;
    }
}
