-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         5.7.24 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura para tabla products.clientes
CREATE TABLE IF NOT EXISTS `clientes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `nombres` varchar(50) NOT NULL,
  `apellidos` varchar(50) NOT NULL,
  `documento` varchar(20) NOT NULL,
  `telefono` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla products.clientes: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` (`id`, `user_id`, `nombres`, `apellidos`, `documento`, `telefono`, `email`, `direccion`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, NULL, 'Juan Guillermo', 'Leal Parra', '1067891941', '(+57)3014993529', 'juanglealp@gmail.com', 'Barrio la Pradera, Montería', '2020-12-12 19:04:42', '2020-12-14 01:42:25', NULL),
	(2, NULL, 'Juan Guillermo', 'Leal Parra', '1067891942', '(+57)3014993527', 'juanglealp2@gmail.com', 'Barrio la Pradera, Montería', '2020-12-14 01:33:54', '2020-12-14 01:33:54', NULL);
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;

-- Volcando estructura para tabla products.facturas
CREATE TABLE IF NOT EXISTS `facturas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `clientes_id` int(11) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `clientes_id` (`clientes_id`),
  CONSTRAINT `clientes_id_fkey` FOREIGN KEY (`clientes_id`) REFERENCES `clientes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla products.facturas: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `facturas` DISABLE KEYS */;
INSERT INTO `facturas` (`id`, `clientes_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, '2020-12-14 08:04:59', '2020-12-14 08:04:59', NULL),
	(2, 2, '2020-12-14 08:05:16', '2020-12-14 08:05:16', NULL);
/*!40000 ALTER TABLE `facturas` ENABLE KEYS */;

-- Volcando estructura para tabla products.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla products.failed_jobs: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Volcando estructura para tabla products.items_factura
CREATE TABLE IF NOT EXISTS `items_factura` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `facturas_id` int(11) unsigned NOT NULL,
  `sku` varchar(50) NOT NULL,
  `cantidad` int(5) unsigned NOT NULL,
  `valor_unitario_producto` double unsigned NOT NULL COMMENT 'Sin IVA',
  `iva` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `subtotal_productos` double unsigned NOT NULL,
  `valor_total_productos` double unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla products.items_factura: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `items_factura` DISABLE KEYS */;
INSERT INTO `items_factura` (`id`, `facturas_id`, `sku`, `cantidad`, `valor_unitario_producto`, `iva`, `subtotal_productos`, `valor_total_productos`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, '1-142577-1', 1, 20000, 0.19, 20000, 23800, '2020-12-14 08:04:59', '2020-12-14 08:04:59', NULL),
	(2, 1, '1-142577-2', 1, 20000, 0.19, 20000, 23800, '2020-12-14 08:04:59', '2020-12-14 08:04:59', NULL),
	(3, 1, '2-658158-1', 2, 75000, 0.19, 150000, 178500, '2020-12-14 08:04:59', '2020-12-14 08:04:59', NULL),
	(4, 2, '1-142577-1', 1, 20000, 0.19, 20000, 23800, '2020-12-14 08:05:16', '2020-12-14 08:05:16', NULL),
	(5, 2, '1-142577-2', 1, 20000, 0.19, 20000, 23800, '2020-12-14 08:05:16', '2020-12-14 08:05:16', NULL),
	(6, 2, '2-658158-2', 2, 75000, 0.19, 150000, 178500, '2020-12-14 08:05:16', '2020-12-14 08:05:16', NULL);
/*!40000 ALTER TABLE `items_factura` ENABLE KEYS */;

-- Volcando estructura para tabla products.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla products.migrations: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
	(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
	(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
	(6, '2016_06_01_000004_create_oauth_clients_table', 1),
	(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
	(8, '2019_08_19_000000_create_failed_jobs_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Volcando estructura para tabla products.oauth_access_tokens
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla products.oauth_access_tokens: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
	('7318f775ec1b5af6ce56218d82a0565783936ee1715ddc72b9bd94c4fb6e60fc614d64ebe2961565', 1, 1, 'Personal Access Token', '[]', 1, '2020-12-11 02:58:28', '2020-12-11 02:58:28', '2020-12-18 02:58:28'),
	('8f532134f51bed3b52b4f6b72d08f3176ed72c9d2cf3d499ca8b058f86210302760d0cedeb3b6735', 1, 1, 'Personal Access Token', '[]', 0, '2020-12-12 23:50:28', '2020-12-12 23:50:28', '2020-12-19 23:50:28'),
	('95b0e72443c0bfb07e850b65b17b312d1429345400edfcfd396f24c9ac1437a1c516b28a2ecaa9bd', 1, 1, 'Personal Access Token', '[]', 0, '2020-12-11 02:54:34', '2020-12-11 02:54:34', '2020-12-18 02:54:34'),
	('ac8db5863bd9ee1653dce84c055303b0a730f041c754581d1d93e6b793c1f5775ef2b68d1e1e5d92', 1, 1, 'Personal Access Token', '[]', 0, '2020-12-12 23:54:11', '2020-12-12 23:54:11', '2020-12-19 23:54:12'),
	('c8d9eb4ab31dcbc642aafbfb2beb7c246a992d1c17e0f68794bfe2c903754c4e35380f2063432de2', 1, 1, 'Personal Access Token', '[]', 0, '2020-12-12 19:02:37', '2020-12-12 19:02:37', '2020-12-19 19:02:37');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;

-- Volcando estructura para tabla products.oauth_auth_codes
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla products.oauth_auth_codes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;

-- Volcando estructura para tabla products.oauth_clients
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla products.oauth_clients: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
	(1, NULL, 'Laravel Personal Access Client', 'RH1zMiWYn7m9UMAbklTPHFLlEIIE3nYRkVJ9J9Mn', NULL, 'http://localhost', 1, 0, 0, '2020-12-11 02:16:38', '2020-12-11 02:16:38'),
	(2, NULL, 'Laravel Password Grant Client', 'QWZq3D6f8V2qCBkfY5IwlzBdto9FrMFpFbfFU7Bk', 'users', 'http://localhost', 0, 1, 0, '2020-12-11 02:16:38', '2020-12-11 02:16:38');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;

-- Volcando estructura para tabla products.oauth_personal_access_clients
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla products.oauth_personal_access_clients: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
	(1, 1, '2020-12-11 02:16:38', '2020-12-11 02:16:38');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;

-- Volcando estructura para tabla products.oauth_refresh_tokens
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla products.oauth_refresh_tokens: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;

-- Volcando estructura para tabla products.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla products.password_resets: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Volcando estructura para tabla products.productos
CREATE TABLE IF NOT EXISTS `productos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `referencia` varchar(50) NOT NULL,
  `descripcion` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla products.productos: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` (`id`, `nombre`, `referencia`, `descripcion`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Casimeta Tipo Polo', '142577', 'La Camiseta para Hombre Tipo Polo le dará un toque formal a tu pinta ya que es manga corta, su silueta es slim fit, tiene cuello camisero con banda, es abierta totalmente en frente mediante botones a la vista. Está confeccionada en algodón polyester para brindar mayor frescura y comodidad. Disponible en nuestra tienda online.', '2020-12-13 03:56:42', '2020-12-13 03:59:14', NULL),
	(2, 'Pantalón Parsons Verde Oliva', '658158', '<ul><li>Pantalón.</li><li>Con bolsillos cargo.</li><li>Slim Fit, semi ajustado.</li><li>En algodón.</li><li>Con cremallera en frente y botón en cintura.</li></ul>', '2020-12-13 15:30:53', '2020-12-13 15:30:53', NULL);
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;

-- Volcando estructura para tabla products.stock
CREATE TABLE IF NOT EXISTS `stock` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `sku` varchar(100) NOT NULL,
  `cantidad` int(10) unsigned DEFAULT '0',
  `precio` double unsigned DEFAULT '0' COMMENT 'Sin IVA',
  `url_foto` varchar(100) DEFAULT NULL,
  `iva` decimal(10,2) DEFAULT '0.00',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `product_id_fkey` FOREIGN KEY (`product_id`) REFERENCES `productos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla products.stock: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `stock` DISABLE KEYS */;
INSERT INTO `stock` (`id`, `product_id`, `sku`, `cantidad`, `precio`, `url_foto`, `iva`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, '1-142577-1', 13, 20000, 'https://loremflickr.com/400/600', 0.19, '2020-12-13 05:19:40', '2020-12-13 05:19:40', NULL),
	(2, 1, '1-142577-2', 15, 20000, 'https://loremflickr.com/400/600', 0.19, '2020-12-13 05:20:12', '2020-12-13 05:20:12', NULL),
	(3, 1, '1-142577-1', 5, 20000, 'https://loremflickr.com/400/600', 0.19, '2020-12-13 05:27:24', '2020-12-13 05:27:24', NULL),
	(4, 2, '2-658158-1', 20, 75000, 'https://loremflickr.com/400/600', 0.19, '2020-12-13 15:31:28', '2020-12-13 15:31:28', NULL),
	(5, 2, '2-658158-2', 30, 75000, 'https://loremflickr.com/400/600', 0.19, '2020-12-13 17:22:00', '2020-12-13 17:22:00', NULL);
/*!40000 ALTER TABLE `stock` ENABLE KEYS */;

-- Volcando estructura para tabla products.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla products.users: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Juan Leal', 'juanglealp@gmail.com', NULL, '$2y$10$7Gop/3Xs/PBncwhruWmKPOGJukkUG733iOi80eYoE1E6nybwXAeJ2', NULL, '2020-12-11 02:47:42', '2020-12-11 02:47:42');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
