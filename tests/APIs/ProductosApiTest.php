<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Productos;

class ProductosApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_productos()
    {
        $productos = Productos::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/productos', $productos
        );

        $this->assertApiResponse($productos);
    }

    /**
     * @test
     */
    public function test_read_productos()
    {
        $productos = Productos::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/productos/'.$productos->id
        );

        $this->assertApiResponse($productos->toArray());
    }

    /**
     * @test
     */
    public function test_update_productos()
    {
        $productos = Productos::factory()->create();
        $editedProductos = Productos::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/productos/'.$productos->id,
            $editedProductos
        );

        $this->assertApiResponse($editedProductos);
    }

    /**
     * @test
     */
    public function test_delete_productos()
    {
        $productos = Productos::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/productos/'.$productos->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/productos/'.$productos->id
        );

        $this->response->assertStatus(404);
    }
}
