<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Facturas;

class FacturasApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_facturas()
    {
        $facturas = Facturas::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/facturas', $facturas
        );

        $this->assertApiResponse($facturas);
    }

    /**
     * @test
     */
    public function test_read_facturas()
    {
        $facturas = Facturas::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/facturas/'.$facturas->id
        );

        $this->assertApiResponse($facturas->toArray());
    }

    /**
     * @test
     */
    public function test_update_facturas()
    {
        $facturas = Facturas::factory()->create();
        $editedFacturas = Facturas::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/facturas/'.$facturas->id,
            $editedFacturas
        );

        $this->assertApiResponse($editedFacturas);
    }

    /**
     * @test
     */
    public function test_delete_facturas()
    {
        $facturas = Facturas::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/facturas/'.$facturas->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/facturas/'.$facturas->id
        );

        $this->response->assertStatus(404);
    }
}
