<?php namespace Tests\Repositories;

use App\Models\Productos;
use App\Repositories\ProductosRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ProductosRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProductosRepository
     */
    protected $productosRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->productosRepo = \App::make(ProductosRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_productos()
    {
        $productos = Productos::factory()->make()->toArray();

        $createdProductos = $this->productosRepo->create($productos);

        $createdProductos = $createdProductos->toArray();
        $this->assertArrayHasKey('id', $createdProductos);
        $this->assertNotNull($createdProductos['id'], 'Created Productos must have id specified');
        $this->assertNotNull(Productos::find($createdProductos['id']), 'Productos with given id must be in DB');
        $this->assertModelData($productos, $createdProductos);
    }

    /**
     * @test read
     */
    public function test_read_productos()
    {
        $productos = Productos::factory()->create();

        $dbProductos = $this->productosRepo->find($productos->id);

        $dbProductos = $dbProductos->toArray();
        $this->assertModelData($productos->toArray(), $dbProductos);
    }

    /**
     * @test update
     */
    public function test_update_productos()
    {
        $productos = Productos::factory()->create();
        $fakeProductos = Productos::factory()->make()->toArray();

        $updatedProductos = $this->productosRepo->update($fakeProductos, $productos->id);

        $this->assertModelData($fakeProductos, $updatedProductos->toArray());
        $dbProductos = $this->productosRepo->find($productos->id);
        $this->assertModelData($fakeProductos, $dbProductos->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_productos()
    {
        $productos = Productos::factory()->create();

        $resp = $this->productosRepo->delete($productos->id);

        $this->assertTrue($resp);
        $this->assertNull(Productos::find($productos->id), 'Productos should not exist in DB');
    }
}
