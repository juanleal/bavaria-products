<?php namespace Tests\Repositories;

use App\Models\Facturas;
use App\Repositories\FacturasRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class FacturasRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var FacturasRepository
     */
    protected $facturasRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->facturasRepo = \App::make(FacturasRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_facturas()
    {
        $facturas = Facturas::factory()->make()->toArray();

        $createdFacturas = $this->facturasRepo->create($facturas);

        $createdFacturas = $createdFacturas->toArray();
        $this->assertArrayHasKey('id', $createdFacturas);
        $this->assertNotNull($createdFacturas['id'], 'Created Facturas must have id specified');
        $this->assertNotNull(Facturas::find($createdFacturas['id']), 'Facturas with given id must be in DB');
        $this->assertModelData($facturas, $createdFacturas);
    }

    /**
     * @test read
     */
    public function test_read_facturas()
    {
        $facturas = Facturas::factory()->create();

        $dbFacturas = $this->facturasRepo->find($facturas->id);

        $dbFacturas = $dbFacturas->toArray();
        $this->assertModelData($facturas->toArray(), $dbFacturas);
    }

    /**
     * @test update
     */
    public function test_update_facturas()
    {
        $facturas = Facturas::factory()->create();
        $fakeFacturas = Facturas::factory()->make()->toArray();

        $updatedFacturas = $this->facturasRepo->update($fakeFacturas, $facturas->id);

        $this->assertModelData($fakeFacturas, $updatedFacturas->toArray());
        $dbFacturas = $this->facturasRepo->find($facturas->id);
        $this->assertModelData($fakeFacturas, $dbFacturas->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_facturas()
    {
        $facturas = Facturas::factory()->create();

        $resp = $this->facturasRepo->delete($facturas->id);

        $this->assertTrue($resp);
        $this->assertNull(Facturas::find($facturas->id), 'Facturas should not exist in DB');
    }
}
